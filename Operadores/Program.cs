﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operadores
{
    class Program
    {
        static void Main(string[] args)
        {
            int dias = 31;
            double salario = 850.50;
            double despesas = 345.89;
            double divisao = salario / dias;
            double multiplicacao = 2 * salario;
            double soma = salario + despesas;
            double subtracao = salario - despesas;
            double resto = salario % dias;

            Console.WriteLine("Divisão: " + divisao);
            Console.WriteLine("Multiplicação: " + multiplicacao);
            Console.WriteLine("Soma: " + soma);
            Console.WriteLine("Subtração: " + subtracao);
            Console.WriteLine("Resto: " + resto);

            Console.ReadKey();
        }
    }
}
